# 1. Prerequisites Setup

- Install Visual Studio 2019 or newer
- Install meson and ninja as described in [GStreamer README](https://gitlab.freedesktop.org/gstreamer/gstreamer#windows-prerequisites-setup)

# 2. Build

This project requires some bug fixes and enhancements made after the 1.24 release.
Either a GStreamer monorepo or Cerbero build of the main branch is required.
This README documents both those methods.

## 2.1 Build alongside the monorepo

- Clone [GStreamer](https://gitlab.freedesktop.org/gstreamer/gstreamer) main branch
  - If you want to build some other branch, you need [this merge request](https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/6404)
- Clone this project inside the GStreamer monorepo's subprojects directory
  - For example, if the monorepo is cloned in C:\Devel\gstreamer, you would run:
  ```
  PS C:\> cd C:\Devel\gstreamer\subprojects
  PS C:\Devel\gstreamer\subprojects> git clone https://gitlab.freedesktop.org/seungha.yang/gst-asio-proxy
  ```

```sh
# Configure GStreamer with ASIO plugin and this subproject enabled
PS C:\Devel\gstreamer> meson builddir -Dcustom_subprojects="gst-asio-proxy" -Dgst-plugins-bad:asio=enabled

# Then, compile
PS C:\Devel\gstreamer> meson compile -C builddir
```

## 2.2. Standalone build using pre-installed MSI

Build GStreamer MSVC MSIs using Cerbero's main branch, and install both the MSIs.

**NOTE**: These two MRs are required if you want to build some other branch of Cerbero.
The official 1.24.0 and 1.24.1 installers don't contain these changes.
- https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/6404
- https://gitlab.freedesktop.org/gstreamer/cerbero/-/merge_requests/1407

Add pre-installed GStreamer `bin` directory to `PATH` environment
```sh
# GStreamer installed path can be varying depending on toolchain type (MSVC vs MinGW)
# and the specified directory during GStreamer package installation
PS C:\Devel> $env:PATH = "C:\gstreamer\1.0\msvc_x86_64\bin;" + $env:Path
```

Clone, configure and build this project:
```sh
PS C:\Devel> git clone https://gitlab.freedesktop.org/seungha.yang/gst-asio-proxy
PS C:\Devel> cd gst-asio-proxy
PS C:\Devel\gst-asio-proxy> meson builddir; meson compile -C builddir
```

# 3. Install ASIO driver

Compiled ASIO proxy driver will be located at `{builddir}/subprojects/gst-asio-proxy/src/client/GstAsioProxy.dll` in case of monorepo build,
and `{builddir}/src/client/GstAsioProxy.dll` in case of standalone build.

1) (optional) Copy `GstAsioProxy.dll` to preferred location
2) Open cmd.exe as admin
3) Register `GstAsioProxy.dll` using `regsvr32.exe`
```sh
regsvr32.exe PATH/TO/GstAsioProxy.dll
```

# 4. Uninstall ASIO driver

1) Open cmd.exe as admin
2) Unregister `GstAsioProxy.dll` using `regsvr32.exe`
```sh
regsvr32.exe PATH/TO/GstAsioProxy.dll /u
```
3) (optional) Delete `GstAsioProxy.dll` if needed. `regsvr32.exe` does not delete the unregistered dll.

# 5. Run

This project consists of two main modules, one is the ASIO proxy server and the
other is the virtual ASIO driver (invoked by the client). For the client
processes (e.g, GStreamer or third-party player) to be able to execute I/O
operations and communicate with each other, the ASIO proxy server must be
launched first.

## 5.1. Launch ASIO proxy server

If you built the project as part of the monorepo, you can open a new terminal
and enter the monorepo's devenv to access everything:

```sh
PS C:\Devel\gstreamer> meson devenv -C builddir

# Display option arguments
[gstreamer-full] PS C:\Devel\gstreamer> gst-asio-proxy-server.exe --help
Usage:
  gst-asio-proxy-server.exe [OPTION…] GStreamer ASIO proxy server

Help Options:
  -h, --help                        Show help options
  --help-all                        Show all help options
  --help-gst                        Show GStreamer Options

Application Options:
  -d, --domain                      PTP domain, if unspecified, system clock will be used
  -i, --interfaces                  (optional) interface names to listen on for PTP packets
  -t, --ttl                         (optional) TTL to use for multicast packets
  -c, --channels                    The number of audio channels, valid range is [1, 16] and default is 2
  -f, --format                      Audio format string (e.g., "S24LE", "F32LE")
```

If you built the project standalone, you can do the same by entering the devenv inside the project:

```sh
PS C:\Devel\gst-asio-proxy> meson devenv -C builddir

# Display option arguments
[gst-asio-proxy] PS C:\Devel\gst-asio-proxy> gst-asio-proxy-server.exe --help
Usage:
  gst-asio-proxy-server.exe [OPTION…] GStreamer ASIO proxy server

Help Options:
  -h, --help                        Show help options
  --help-all                        Show all help options
  --help-gst                        Show GStreamer Options

Application Options:
  -d, --domain                      PTP domain, if unspecified, system clock will be used
  -i, --interfaces                  (optional) interface names to listen on for PTP packets
  -t, --ttl                         (optional) TTL to use for multicast packets
  -c, --channels                    The number of audio channels, valid range is [1, 16] and default is 2
  -f, --format                      Audio format string (e.g., "S24LE", "F32LE")
```

By default, the server process runs with system clock and in stereo mode.

To enable the PTP clock, the `--domain` option should be specified, and the number of audio channels is configurable via the `--channels` option

```sh
# Launch server in system clock mode, enable up to 16 channels, and wait for server ready message "Using system clock"
[gstreamer-full] PS C:\Devel\gstreamer> gst-asio-proxy-server.exe -c 16
Selected audio format F32LE
Using system clock

# Launch server in system clock mode, enable up to 2 channels (default)
# with S24LE audio format (default is F32LE), and wait for server ready message "Using system clock"
[gstreamer-full] PS C:\Devel\gstreamer> gst-asio-proxy-server.exe -f S24LE
Selected audio format S24LE
Using system clock

# Launch server in PTP clock mode, and wait for server ready message
# "Using PTP clock". It might take a few seconds for PTP sync.
[gstreamer-full] PS C:\Devel\gstreamer> gst-asio-proxy-server.exe -d 0
Selected audio format F32LE
Using PTP clock
```

## 5.2 Launch GStreamer ASIO pipeline (record pipeline)

Unless `channels` is specified via caps, GstAsioSrc element will read all channel data supported by driver. The channels to read can be selected via the `input-channels` property of GstAsioSrc.

**NOTE** : `occupy-all-channels` property of GstAsioSrc and GstAsioSink is enabled by default, to support multiple src/sink use case. However that requires unnecessary processing power if only single src or sink element is needed in the process. Set `occupy-all-channels=false` is recommended in case of single src or sink use case.

```sh
# Record audio from channel "0" and "3", and produce stereo audio stream
[gstreamer-full] PS C:\Devel\gstreamer> gst-launch.exe asiosrc input-channels="0,3" device-clsid="\{EB7CA4B0-5491-47DA-9B3F-E8EF55794039\}" occupy-all-channels=false ! queue ! audioconvert ! audioresample ! wasapi2sink
```

## 5.3 Launch producer application

Any application which supports ASIO can feed audio data through this virtual ASIO driver, you just have to select it. The driver is called "GStreamer ASIO Proxy".

An example GStreamer pipeline is
```sh
# Feed stereo test audio stream to virtual asio
[gstreamer-full] PS C:\Devel\gstreamer> gst-launch.exe audiotestsrc is-live=true ! queue ! asiosink output-channels="0,1" device-clsid="\{EB7CA4B0-5491-47DA-9B3F-E8EF55794039\}" occupy-all-channels=false
```

Testing has also been done with Foobar2000.

# 6. Deployment

- `gst-asio-proxy-server.exe`: Deploy this like any GStreamer application. This executable can be located in any place as long as the `gst-asio-proxy-server.exe` can find `gstreamer-1.0.dll` from DLL search paths.

- `GstAsioProxy.dll`: This is a standalone library relying only on system libraries. It can be located anywhere but once it's registered via `regsvr32.exe`, the dll should stay in that location. Once the dll is moved to another place, re-registration is required.

- `gstasio.dll`: Needs to be installed in the usual GStreamer plugin path or the `GST_PLUGIN_PATH` environment variable should contain the directory where `gstasio.dll` is located.
