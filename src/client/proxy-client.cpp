#include "proxy-client.h"
#include "logger.h"
#include <thread>
#include <avrt.h>
#include <timeapi.h>
#include <string.h>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <vector>
#include <stdio.h>
#include "../protocol.h"

static const double twoRaisedTo32 = 4294967296.;

struct ChannelData
{
  void *in_buffer[2];
  void *out_buffer[2];
};

struct AsioProxyClient : OVERLAPPED
{
  AsioProxyClient ()
  {
    OVERLAPPED *p = static_cast<OVERLAPPED *> (this);
    p->Internal = 0;
    p->InternalHigh = 0;
    p->Offset = 0;
    p->OffsetHigh = 0;

    cancellable = CreateEvent (nullptr, TRUE, FALSE, nullptr);
  }

  ~AsioProxyClient ()
  {
    SetEvent (cancellable);
    if (server_handle)
      CloseHandle (server_handle);

    if (loop_thread) {
      loop_thread->join ();
      delete loop_thread;
    }

    if (mapped_buf)
      UnmapViewOfFile (mapped_buf);

    if (mmf_handle)
      CloseHandle (mmf_handle);

    if (loop_thread_handle)
      CloseHandle (loop_thread_handle);

    CloseHandle (cancellable);
    free (address);
  }

  HANDLE pipe = INVALID_HANDLE_VALUE;
  HANDLE server_handle = nullptr;
  HANDLE loop_thread_handle = nullptr;
  void *mapped_buf = nullptr;
  ChannelData buffers[kMaxChannels] = { };
  UINT num_channels = 0;
  HANDLE cancellable;
  ASIOCallbacks callback = { };
  std::vector<ASIOBufferInfo> buffer_infos;
  wchar_t *address = nullptr;
  MMFData mmf_data;
  HANDLE mmf_handle = nullptr;
  std::atomic<bool> aborted = { false };
  std::atomic<bool> running = { false };
  bool need_preroll = true;
  bool pipe_busy = false;
  ASIOTimeStamp system_time = { };
  ASIOSamples sample_pos = { };
  std::vector<ASIOChannelInfo> active_channel_info;
  UINT64 start_server_idx = (UINT64) -1;
  UINT64 cur_server_idx = (UINT64) -1;
  std::thread *loop_thread = nullptr;
  std::mutex lock;
  std::condition_variable cond;
  ClientMsg client_msg;
  UINT64 server_msg;
};

static void WINAPI
clock_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap);

static void WINAPI
switch_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto self = (AsioProxyClient *) overlap;

  if (error_code != ERROR_SUCCESS) {
    ASIO_PROXY_ERROR ("Couldn't read clock msg");
    SetEvent (self->cancellable);
    return;
  }

  self->pipe_busy = false;
}

static void WINAPI
clock_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto self = (AsioProxyClient *) overlap;

  if (error_code != ERROR_SUCCESS) {
    ASIO_PROXY_ERROR ("Couldn't read clock msg");
    SetEvent (self->cancellable);
    return;
  }

  ASIO_PROXY_DEBUG ("Server clock tick %llu", self->server_msg);

  auto now = (double)((unsigned long)timeGetTime ()) * 1000000.;
  self->system_time.hi = (unsigned long) (now / twoRaisedTo32);
  self->system_time.lo =
      (unsigned long) (now - (self->system_time.hi * twoRaisedTo32));

  self->cur_server_idx = self->server_msg;

  if (self->running && self->callback.buffer_switch) {
    if (self->start_server_idx == (UINT64) -1)
      self->start_server_idx = self->server_msg;

    long idx = self->server_msg % 2;
    if (!self->need_preroll) {
      if (self->pipe_busy) {
        ASIO_PROXY_WARNING ("Pipe is pipe_busy");
      } else {
        // TODO: multi ch support
        self->client_msg.type = AsioProxyPktType::PktTypeSwitch;
        for (const auto & it : self->buffer_infos) {
          if (it.is_input) {
            UINT64 ch_field = ((UINT64) 1) << (3 + it.channel_num * 2);
            self->client_msg.type |= (AsioProxyPktType) ch_field;
          } else {
            UINT64 ch_field = ((UINT64) 1) << (4 + it.channel_num * 2);
            self->client_msg.type |= (AsioProxyPktType) ch_field;
          }
        }
        self->client_msg.payload = self->server_msg - 1;
        self->pipe_busy = true;
        if (!WriteFileEx (self->pipe, &self->client_msg,
            sizeof (self->client_msg), self, switch_finished)) {
          ASIO_PROXY_ERROR ("WriteFileEx failed");
          SetEvent (self->cancellable);
          return;
        }
      }
    }

    self->callback.buffer_switch (idx, ASIOFalse);
    self->need_preroll = false;
    double sample_pos = (double) (self->cur_server_idx + 1 - self->start_server_idx)
        * kBlockSize;
    if (sample_pos >= twoRaisedTo32) {
      self->sample_pos.hi = (unsigned long) (sample_pos / twoRaisedTo32);
      self->sample_pos.lo =
          (unsigned long) (sample_pos - (self->sample_pos.hi * twoRaisedTo32));
    } else {
      self->sample_pos.hi = 0;
      self->sample_pos.lo = (unsigned long) sample_pos;
    }
  } else {
    // TODO: do this at stop()
    self->sample_pos.hi = 0;
    self->sample_pos.lo = 0;
    self->need_preroll = true;
    self->start_server_idx = (UINT64) -1;
    self->active_channel_info.clear ();
  }

  if (!ReadFileEx (self->pipe, &self->server_msg, sizeof (self->server_msg),
      overlap, clock_finished)) {
    ASIO_PROXY_ERROR ("ReadFileEx failed");
    SetEvent (self->cancellable);
  }
}

static void WINAPI
start_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto self = (AsioProxyClient *) overlap;

  if (error_code != ERROR_SUCCESS) {
    ASIO_PROXY_ERROR ("Connection failed");
    SetEvent (self->cancellable);
    return;
  }

  ASIO_PROXY_DEBUG ("Sent start command");
  if (!ReadFileEx (self->pipe, &self->server_msg, sizeof (self->server_msg),
      overlap, clock_finished)) {
    ASIO_PROXY_ERROR ("ReadFileEx failed");
    SetEvent (self->cancellable);
  }
}

static void WINAPI
conn_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto self = (AsioProxyClient *) overlap;

  if (error_code != ERROR_SUCCESS) {
    ASIO_PROXY_ERROR ("Connection failed");
    SetEvent (self->cancellable);
    return;
  }

  self->server_handle = OpenProcess (PROCESS_DUP_HANDLE, FALSE,
      self->mmf_data.pid);
  if (!self->server_handle) {
    ASIO_PROXY_ERROR ("Couldn't open server process");
    SetEvent (self->cancellable);
    return;
  }

  if (!DuplicateHandle (self->server_handle, self->mmf_data.mmf_handle,
      GetCurrentProcess (), &self->mmf_handle, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
    ASIO_PROXY_ERROR ("Couldn't duplicate MMF handle");
    SetEvent (self->cancellable);
    return;
  }

  {
    std::lock_guard <std::mutex> lk (self->lock);

    self->mapped_buf = MapViewOfFile (self->mmf_handle, FILE_MAP_ALL_ACCESS,
        0, 0, self->mmf_data.mmf_size);
    if (!self->mapped_buf) {
      ASIO_PROXY_ERROR ("Couldn't map MMF handle");
      SetEvent (self->cancellable);
      return;
    }

    self->num_channels = self->mmf_data.num_channels;
    memset (self->mapped_buf, 0, self->mmf_data.mmf_size);

    auto ch_buf_size = self->mmf_data.mmf_size / (self->num_channels * 4);
    auto data = (UINT8 *) self->mapped_buf;
    for (UINT i = 0; i < self->num_channels; i++) {
      self->buffers[i].in_buffer[0] = data;
      data += ch_buf_size;
      self->buffers[i].in_buffer[1] = data;
      data += ch_buf_size;

      self->buffers[i].out_buffer[0] = data;
      data += ch_buf_size;
      self->buffers[i].out_buffer[1] = data;
      data += ch_buf_size;
    }

    self->cond.notify_all ();
  }

  ASIO_PROXY_DEBUG ("Connection made");
  self->client_msg.type = AsioProxyPktType::PktTypeStart;
  if (!WriteFileEx (self->pipe, &self->client_msg, sizeof (self->client_msg),
      self, start_finished)) {
    ASIO_PROXY_ERROR ("WriteFileEx failed");
    SetEvent (self->cancellable);
  }
}

static void
loop_thread_func (AsioProxyClient * self)
{
  DWORD task_idx = 0;
  auto task_handle = AvSetMmThreadCharacteristicsA ("Pro Audio", &task_idx);
  DWORD mode = PIPE_READMODE_MESSAGE;
  HANDLE pipe;
  auto cur_thread = GetCurrentThread ();

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
  CREATEFILE2_EXTENDED_PARAMETERS params;
  memset (&params, 0, sizeof (CREATEFILE2_EXTENDED_PARAMETERS));
  params.dwSize = sizeof (CREATEFILE2_EXTENDED_PARAMETERS);
  params.dwFileAttributes = 0;
  params.dwFileFlags = FILE_FLAG_OVERLAPPED;
  params.dwSecurityQosFlags = SECURITY_IMPERSONATION;
#endif

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
  pipe = CreateFile2 (self->address, GENERIC_READ | GENERIC_WRITE, 0,
        OPEN_EXISTING, &params);
#else
  pipe = CreateFileW (self->address,
        GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED, nullptr);
#endif

  if (pipe == INVALID_HANDLE_VALUE) {
    ASIO_PROXY_ERROR ("Couldn't open pipe");
    goto out;
  }

  if (!SetNamedPipeHandleState (pipe, &mode, nullptr, nullptr)) {
    ASIO_PROXY_ERROR ("SetNamedPipeHandleState failed");
    goto out;
  }

  ASIO_PROXY_DEBUG ("Waiting for initial connection");
  self->pipe = pipe;
  if (!ReadFileEx (pipe, &self->mmf_data, sizeof (self->mmf_data),
      self, conn_finished)) {
    ASIO_PROXY_ERROR ("ReadFileEx failed");
    goto out;
  }

  DuplicateHandle (GetCurrentProcess(), cur_thread,
      GetCurrentProcess(), &self->loop_thread_handle, 0, FALSE,
      DUPLICATE_SAME_ACCESS);

  do {
    auto ret = WaitForSingleObjectEx (self->cancellable, INFINITE, TRUE);

    switch (ret) {
      case WAIT_IO_COMPLETION:
        break;
      default:
        goto out;
    }
  } while (true);

out:
  ASIO_PROXY_DEBUG ("Exit loop");

  {
    std::lock_guard <std::mutex> lk (self->lock);
    self->aborted = true;
    self->cond.notify_all ();
  }

  if (pipe != INVALID_HANDLE_VALUE) {
    CancelIo (pipe);
    CloseHandle (pipe);
  }

  if (task_handle)
    AvRevertMmThreadCharacteristics (task_handle);
}

AsioProxyClient *
asio_proxy_client_new (const wchar_t * address)
{
  auto self = new AsioProxyClient ();

  self->address = _wcsdup (address);
  self->loop_thread = new std::thread (loop_thread_func, self);
  self->cancellable = CreateEvent (nullptr, TRUE, FALSE, nullptr);

  {
    std::unique_lock <std::mutex> lk (self->lock);
    while (!self->aborted && !self->mapped_buf)
      self->cond.wait (lk);
  }

  if (self->aborted) {
    delete self;
    return nullptr;
  }

  return self;
}

void
asio_proxy_client_free (AsioProxyClient * client)
{
  if (!client)
    return;

  delete client;
}

struct CreateBufferData
{
  HANDLE event_handle = nullptr;
  AsioProxyClient *self;
  ASIOBufferInfo *infos;
  long num_channels;
  long buffer_size;
  ASIOCallbacks * callbacks;
  ASIOError ret;
};

static void NTAPI
create_buffer_cb (ULONG_PTR param)
{
  auto data = (CreateBufferData *) param;
  auto self = data->self;

  self->callback = *data->callbacks;

  self->buffer_infos.clear ();
  self->active_channel_info.clear ();
  for (long i = 0; i < data->num_channels; i++) {
    auto info = &data->infos[i];
    if (info->channel_num >= self->num_channels) {
      ASIO_PROXY_ERROR ("Invalid channel index %d", info->channel_num);
      data->ret = ASE_NotPresent;
      break;
    }

    if (info->is_input) {
      ASIO_PROXY_DEBUG ("Creating input ch %d", info->channel_num);
      info->buffers[0] = self->buffers[info->channel_num].in_buffer[0];
      info->buffers[1] = self->buffers[info->channel_num].in_buffer[1];
    } else {
      ASIO_PROXY_DEBUG ("Creating output ch %d", info->channel_num);
      info->buffers[0] = self->buffers[info->channel_num].out_buffer[0];
      info->buffers[1] = self->buffers[info->channel_num].out_buffer[1];
    }

    self->buffer_infos.push_back (*info);

    ASIOChannelInfo ch_info;
    ch_info.channel = info->channel_num;
    ch_info.is_active = ASIOTrue;
    ch_info.is_input = info->is_input;
    ch_info.channel_group = 0;
    ch_info.type = self->mmf_data.asio_format;
    self->active_channel_info.push_back (ch_info);
  }

  data->ret = ASE_OK;

  ASIO_PROXY_DEBUG ("Create buffer called");

  SetEvent (data->event_handle);
}

ASIOError
asio_proxy_client_create_bufffer (AsioProxyClient * client,
    ASIOBufferInfo * infos, long num_channels, long buffer_size,
    ASIOCallbacks * callbacks)
{
  if (!client || !infos || num_channels <= 0 ||
      num_channels > 2 * kMaxChannels || buffer_size != kBlockSize ||
      !callbacks) {
    ASIO_PROXY_ERROR ("Invalid param");
    return ASE_InvalidParameter;
  }

  CreateBufferData data = {};
  data.event_handle = CreateEvent (nullptr, TRUE, FALSE, nullptr);
  data.self = client;
  data.infos = infos;
  data.num_channels = num_channels;
  data.callbacks = callbacks;

  HANDLE waitables[2] = { data.event_handle, client->cancellable };
  if (!QueueUserAPC (create_buffer_cb, client->loop_thread_handle,
        (ULONG_PTR) &data)) {
    ASIO_PROXY_ERROR ("Couldn't create buffer");
    CloseHandle (data.event_handle);
    return ASE_NotPresent;
  }

  auto ret = WaitForMultipleObjectsEx (2, waitables, FALSE, INFINITE, FALSE);
  CloseHandle (data.event_handle);

  if (ret != WAIT_OBJECT_0) {
    ASIO_PROXY_ERROR ("Unexpected return %d", ret);
    return ASE_NotPresent;
  }

  return data.ret;
}

ASIOError
asio_proxy_client_dispose_bufffer (AsioProxyClient * client)
{
  return ASE_OK;
}

ASIOError
asio_proxy_client_start (AsioProxyClient * client)
{
  client->running = true;

  return ASE_OK;
}

ASIOError
asio_proxy_client_stop (AsioProxyClient * client)
{
  client->running = false;

  return ASE_OK;
}

ASIOError
asio_proxy_client_get_sample_position (AsioProxyClient * client,
    ASIOSamples * pos, ASIOTimeStamp * timestamp)
{
  if (timestamp)
    *timestamp = client->system_time;

  if (pos)
    *pos = client->sample_pos;

  return ASE_OK;
}

ASIOError
asio_proxy_client_get_channel_info (AsioProxyClient * client,
    ASIOChannelInfo * info)
{
  if (!info || info->channel < 0 || info->channel >= kMaxChannels)
    return ASE_InvalidParameter;

  info->channel_group = 0;
  info->is_active = ASIOFalse;
  info->type = client->mmf_data.asio_format;

  for (const auto & it : client->active_channel_info) {
    if (it.channel != info->channel || it.is_input != info->is_input)
      continue;

    info->is_active = it.is_active;
    break;
  }

  sprintf_s (info->name, 32, "%s-%d", info->is_input ? "Input" : "Output",
      info->channel);
  return ASE_OK;
}

ASIOError
asio_proxy_client_get_channels (AsioProxyClient * client, long * in_ch,
    long * out_ch)
{
  if (in_ch)
    *in_ch = client->num_channels;
  if (out_ch)
    *out_ch = client->num_channels;

  return ASE_OK;
}