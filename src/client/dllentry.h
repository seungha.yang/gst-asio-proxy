#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

BOOL WINAPI
DllMain (HINSTANCE module, DWORD reason, LPVOID reserved);

HRESULT WINAPI
DllRegisterServer ();

HRESULT WINAPI
DllUnregisterServer ();

#ifdef __cplusplus
}
#endif
