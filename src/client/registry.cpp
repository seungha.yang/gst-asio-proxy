#include "registry.h"
#include <string>
#include <string.h>

static BOOL
regPathExists (HKEY key, const wchar_t * subkey)
{
  HKEY ret = nullptr;

  auto status = RegOpenKeyExW (key, subkey, 0, KEY_READ, &ret);
  if (status == ERROR_SUCCESS) {
    RegCloseKey (ret);
    return TRUE;
  }

  return FALSE;
}

static BOOL
delRegNode (HKEY key, const wchar_t * subkey)
{
  RegDeleteTreeW (key, subkey);
  RegDeleteKeyW (key, subkey);

  return TRUE;
}

static BOOL
createRegPath (HKEY root_key, const wchar_t * parent, const wchar_t * path)
{
  std::wstring new_path = std::wstring (parent) + L"\\" + std::wstring (path);

  if (regPathExists (root_key, new_path.c_str ()))
    return TRUE;

  HKEY parent_key;
  auto status = RegOpenKeyW (root_key, parent, &parent_key);
  if (status != ERROR_SUCCESS)
    return FALSE;

  HKEY subkey;
  status = RegCreateKeyW (parent_key, path, &subkey);
  if (status == ERROR_SUCCESS)
    RegCloseKey (subkey);

  RegCloseKey (parent_key);

  return status == ERROR_SUCCESS;
}

static BOOL
setRegString (HKEY root_key, const wchar_t * path, const wchar_t * name,
    const wchar_t * value)
{
  HKEY subkey;
  auto status = RegOpenKeyW (root_key, path, &subkey);
  if (status != ERROR_SUCCESS)
    return FALSE;

  status = RegSetValueExW (subkey, name, 0, REG_SZ, (const BYTE *) value,
      (wcslen (value) + 1) * sizeof (wchar_t));
  RegCloseKey (subkey);

  return status == ERROR_SUCCESS;
}

static BOOL
unregisterDriver (const wchar_t * clsid_str, const wchar_t * name)
{
  auto class_path = L"CLSID\\" + std::wstring (clsid_str);
  delRegNode (HKEY_CLASSES_ROOT, class_path.c_str ());

  auto machine_path = std::wstring (L"SOFTWARE\\ASIO\\") + name;
  delRegNode (HKEY_LOCAL_MACHINE, machine_path.c_str ());

  return TRUE;
}

BOOL
UnregisterDriver (CLSID clsid, const wchar_t * name)
{
  LPOLESTR clsid_str = nullptr;
  auto hr = StringFromCLSID (clsid, &clsid_str);
  if (FAILED (hr))
    return FALSE;

  auto ret = unregisterDriver (clsid_str, name);
  CoTaskMemFree (clsid_str);

  return ret;
}

BOOL
RegisterDriver (CLSID clsid, const wchar_t * dll_name,
    const wchar_t * name, const wchar_t * desc, const wchar_t * apartment)
{
  auto handle = GetModuleHandleW (dll_name);
  if (!handle)
    return FALSE;

  wchar_t dll_fullpath[MAX_PATH * 2] = { };
  if (!GetModuleFileNameW (handle, dll_fullpath, MAX_PATH * 2))
    return FALSE;

  CharLowerW (dll_fullpath);

  LPOLESTR clsid_str = nullptr;
  auto hr = StringFromCLSID (clsid, &clsid_str);
  if (FAILED (hr))
    return FALSE;

  /* Delete old registry */
  unregisterDriver (clsid_str, name);

  auto subpath = L"CLSID\\" + std::wstring (clsid_str);
  createRegPath (HKEY_CLASSES_ROOT, L"CLSID", clsid_str);
  setRegString (HKEY_CLASSES_ROOT, subpath.c_str(), nullptr, desc);

  auto proc_path = subpath + L"\\InProcServer32";
  createRegPath (HKEY_CLASSES_ROOT, subpath.c_str (), L"InProcServer32");
  setRegString (HKEY_CLASSES_ROOT, proc_path.c_str(), nullptr, dll_fullpath);
  setRegString (HKEY_CLASSES_ROOT, proc_path.c_str(), L"ThreadingModel",
      apartment);

  auto asio_path = L"SOFTWARE\\ASIO";
  auto asio_driver_path =
      std::wstring (asio_path) + L"\\" + std::wstring (name);
  createRegPath (HKEY_LOCAL_MACHINE, L"SOFTWARE", L"ASIO");
  createRegPath (HKEY_LOCAL_MACHINE, asio_path, name);
  setRegString (HKEY_LOCAL_MACHINE, asio_driver_path.c_str (), L"CLSID",
      clsid_str);
  setRegString (HKEY_LOCAL_MACHINE, asio_driver_path.c_str (), L"Description",
      desc);

  CoTaskMemFree (clsid_str);

  return TRUE;
}

