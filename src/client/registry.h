#pragma once

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

BOOL
RegisterDriver (CLSID clsid,
                const wchar_t * dll_name,
                const wchar_t * name,
                const wchar_t * desc,
                const wchar_t * apartment);

BOOL
UnregisterDriver (CLSID clsid,
                  const wchar_t * name);

#ifdef __cplusplus
}
#endif