#include "logger.h"
#include <stdlib.h>
#include <string.h>
#include <mutex>

static FILE *g_log_file = nullptr;
static AsioProxyDebugLevel g_log_level = ASIO_PROXY_LEVEL_ERROR;
static DWORD g_pid = 0;
static LARGE_INTEGER g_freq;
static LARGE_INTEGER g_init_time;
static const UINT64 kSecond = 1000000000;

void
asio_proxy_logger_init ()
{
  g_log_file = stderr;
  wchar_t *env;
  char *level_env;

  g_pid = GetCurrentProcessId ();
  QueryPerformanceFrequency (&g_freq);
  QueryPerformanceCounter (&g_init_time);

  env = _wgetenv (L"ASIO_PROXY_DEBUG_FILE");
  if (env) {
    g_log_file = _wfopen (env, L"wb");
    if (!g_log_file)
      g_log_file = stderr;
  }

  level_env = getenv ("ASIO_PROXY_DEBUG");
  if (level_env) {
    if (strcmp (level_env, "0") == 0)
      g_log_level = ASIO_PROXY_LEVEL_NONE;
    else if (strcmp (level_env, "1") == 0)
      g_log_level = ASIO_PROXY_LEVEL_ERROR;
    else if (strcmp (level_env, "2") == 0)
      g_log_level = ASIO_PROXY_LEVEL_WARNING;
    else
      g_log_level = ASIO_PROXY_LEVEL_DEBUG;
  }
}

static const char *
debug_level_to_string (AsioProxyDebugLevel level)
{
  switch (level) {
    case ASIO_PROXY_LEVEL_NONE:
      return "NONE   \t";
    case ASIO_PROXY_LEVEL_ERROR:
      return "ERROR  \t";
    case ASIO_PROXY_LEVEL_WARNING:
      return "WARNING\t";
    default:
      break;
  }

  return "DEBUG  \t";
}

#define TIME_ARGS(t) \
    (UINT) ((t) / (kSecond * 60 * 60)), \
    (UINT) (((t) / (kSecond * 60)) % 60), \
    (UINT) (((t) / kSecond) % 60), \
    (UINT) ((t) % kSecond)

void
asio_proxy_logger_print (AsioProxyDebugLevel level, const char * file,
    const char * function, int line, const char * format, ...)
{
  static std::mutex g_lock;

  if (level > g_log_level || !g_log_file)
    return;

  LARGE_INTEGER now;
  QueryPerformanceCounter (&now);
  UINT64 now_time = (UINT64) (((now.QuadPart - g_init_time.QuadPart) /
      (double) g_freq.QuadPart) * kSecond);

  auto tid = GetCurrentThreadId ();

  auto base = strrchr (file, '\\');
  auto q = strrchr (file, '/');
  if (!base || (q && q > base))
    base = q;

  if (base)
    file = base + 1;

  std::lock_guard <std::mutex> lk (g_lock);

  fprintf (g_log_file, "%u:%02u:%02u.%09u %5lu %5lu %s %s:%d:%s: ",
      TIME_ARGS (now_time), g_pid, tid,
      debug_level_to_string (level), file, line, function);

  va_list var_args;
  va_start (var_args, format);
  vfprintf (g_log_file, format, var_args);
  va_end (var_args);
  fputc ('\n', g_log_file);
  fflush (g_log_file);
}