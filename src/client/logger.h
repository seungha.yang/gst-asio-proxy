#pragma once

#include <windows.h>
#include <stdio.h>
#include <stdarg.h>

enum AsioProxyDebugLevel
{
  ASIO_PROXY_LEVEL_NONE,
  ASIO_PROXY_LEVEL_ERROR,
  ASIO_PROXY_LEVEL_WARNING,
  ASIO_PROXY_LEVEL_DEBUG,
};

void asio_proxy_logger_init ();

void asio_proxy_logger_print (AsioProxyDebugLevel level,
                              const char * file,
                              const char * function,
                              int line,
                              const char * format,
                              ...);

#define ASIO_PROXY_ERROR(...) \
  do { \
    asio_proxy_logger_print (ASIO_PROXY_LEVEL_ERROR, \
        __FILE__, (const char *) __FUNCTION__, __LINE__, __VA_ARGS__); \
  } while (0)

#define ASIO_PROXY_WARNING(...) \
  do { \
    asio_proxy_logger_print (ASIO_PROXY_LEVEL_WARNING, \
        __FILE__, (const char *) __FUNCTION__, __LINE__, __VA_ARGS__); \
  } while (0)

#define ASIO_PROXY_DEBUG(...) \
  do { \
    asio_proxy_logger_print (ASIO_PROXY_LEVEL_DEBUG, \
        __FILE__, (const char *) __FUNCTION__, __LINE__, __VA_ARGS__); \
  } while (0)