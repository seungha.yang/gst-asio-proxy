#pragma once

#include <windows.h>

typedef long ASIOBool;
typedef long ASIOError;
typedef double ASIOSampleRate;
typedef long ASIOSampleType;

enum
{
  ASIOFalse = 0,
  ASIOTrue = 1,
};

enum
{
  ASE_OK = 0,
  ASE_SUCCESS = 0x3f4847a0,
  ASE_NotPresent = -1000,
  ASE_HWMalfunction,
  ASE_InvalidParameter,
  ASE_InvalidMode,
  ASE_SPNotAdvancing,
  ASE_NoClock,
  ASE_NoMemory,
};

struct ASIOBufferInfo
{
  ASIOBool is_input;
  long channel_num;
  void *buffers[2];
};

struct ASIOTimeStamp
{
  unsigned long hi;
  unsigned long lo;
};

struct ASIOSamples
{
  unsigned long hi;
  unsigned long lo;
};

struct ASIOTimeInfo
{
  double speed;
  ASIOTimeStamp system_time;
  ASIOSamples position;
  double rate;
  unsigned long flags;
  char reserved[12];
};

struct ASIOTimeCode
{
  double speed;
  ASIOSamples samples;
  unsigned long flags;
  char future[64];
};

struct ASIOTime
{
  long reserved[4];
  ASIOTimeInfo time_info;
  ASIOTimeCode time_code;
};

struct ASIOClockSource
{
  long index;
  long associated_channel;
  long associated_group;
  ASIOBool isCurrentSource;
  char name[32];
};

struct ASIOChannelInfo
{
  long channel;
  ASIOBool is_input;
  ASIOBool is_active;
  long channel_group;
  ASIOSampleType type;
  char name[32];
};

struct ASIOCallbacks
{
  void (*buffer_switch) (long idx,
                         ASIOBool direct_process);

  void (*rate_changed)  (double rate);

  void (*message)       (long selector,
                         long value,
                         void * message,
                         double * opt);

  ASIOTime *(buffer_switch_time_info) (ASIOTime * params,
                                       long idx,
                                       ASIOBool direct_process);
};

struct IAsio : public IUnknown
{
  virtual ASIOBool init (void *handle) = 0;
  virtual void getDriverName (char *name) = 0;
  virtual long getDriverVersion () = 0;
  virtual void getErrorMessage (char *string) = 0;
  virtual ASIOError start () = 0;
  virtual ASIOError stop () = 0;
  virtual ASIOError getChannels (long *numInputChannels,
      long *numOutputChannels) = 0;
  virtual ASIOError getLatencies (long *inputLatency, long *outputLatency) = 0;
  virtual ASIOError getBufferSize (long *minSize, long *maxSize,
      long *preferredSize, long *granularity) = 0;
  virtual ASIOError canSampleRate (ASIOSampleRate sampleRate) = 0;
  virtual ASIOError getSampleRate (ASIOSampleRate *sampleRate) = 0;
  virtual ASIOError setSampleRate (ASIOSampleRate sampleRate) = 0;
  virtual ASIOError getClockSources (ASIOClockSource *clocks,
      long *numSources) = 0;
  virtual ASIOError setClockSource (long reference) = 0;
  virtual ASIOError getSamplePosition (ASIOSamples *sPos,
      ASIOTimeStamp *tStamp) = 0;
  virtual ASIOError getChannelInfo (ASIOChannelInfo *info) = 0;
  virtual ASIOError createBuffers (ASIOBufferInfo *bufferInfos, long numChannels,
      long bufferSize, ASIOCallbacks *callbacks) = 0;
  virtual ASIOError disposeBuffers () = 0;
  virtual ASIOError controlPanel () = 0;
  virtual ASIOError future (long selector,void *opt) = 0;
  virtual ASIOError outputReady () = 0;
};