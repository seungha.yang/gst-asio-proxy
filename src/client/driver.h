#pragma once

#include <windows.h>
#include "asio.h"
#include "proxy-client.h"

#ifndef INITGUID
#include <initguid.h>
#endif

DEFINE_GUID (IID_ASIO_DRIVER, 0xeb7ca4b0, 0x5491,
    0x47da, 0x9b, 0x3f, 0xe8, 0xef, 0x55, 0x79, 0x40, 0x39);

class IAsioDriver : public IAsio
{
public:
  /* IUnknown */
  STDMETHODIMP QueryInterface (REFIID riid, void ** object);
  STDMETHODIMP_ (ULONG) AddRef ();
  STDMETHODIMP_ (ULONG) Release ();

  /* IAsio */
  ASIOBool init (void *handle);
  void getDriverName (char *name);
  long getDriverVersion ();
  void getErrorMessage (char *string);
  ASIOError start ();
  ASIOError stop ();
  ASIOError getChannels (long *numInputChannels, long *numOutputChannels);
  ASIOError getLatencies (long *inputLatency, long *outputLatency);
  ASIOError getBufferSize (long *minSize, long *maxSize,
    long *preferredSize, long *granularity);
  ASIOError canSampleRate (ASIOSampleRate sampleRate);
  ASIOError getSampleRate (ASIOSampleRate *sampleRate);
  ASIOError setSampleRate (ASIOSampleRate sampleRate);
  ASIOError getClockSources (ASIOClockSource *clocks, long *numSources);
  ASIOError setClockSource (long reference);
  ASIOError getSamplePosition (ASIOSamples *sPos, ASIOTimeStamp *tStamp);
  ASIOError getChannelInfo (ASIOChannelInfo *info);
  ASIOError createBuffers (ASIOBufferInfo *bufferInfos, long numChannels,
    long bufferSize, ASIOCallbacks *callbacks);
  ASIOError disposeBuffers ();
  ASIOError controlPanel ();
  ASIOError future (long selector,void *opt);
  ASIOError outputReady();

  static HRESULT CreateInstance (IAsioDriver ** driver);
  virtual ~IAsioDriver();

private:
  ULONG refcount_ = 1;
  AsioProxyClient * client_ = nullptr;
  char error_msg_[128] = { };
};