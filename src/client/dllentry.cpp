#pragma once

#include <windows.h>
#include "logger.h"
#include "registry.h"
#include "driver.h"
#include "dllentry.h"

#define DLL_NAME L"GstAsioProxy.dll"
#define DRIVER_NAME L"GStreamer ASIO Proxy Driver"
#define DRIVER_DESC L"GStreamer ASIO Proxy"
#define DRIVER_APARTMENT L"Apartment"

struct IAsioClassFactory : public IClassFactory
{
  STDMETHODIMP QueryInterface (REFIID riid, void ** object)
  {
    if (riid == IID_IUnknown || riid == IID_IClassFactory) {
      *object = this;
      AddRef ();
      return S_OK;
    }

    return E_NOINTERFACE;
  }

  STDMETHODIMP_(ULONG) AddRef ()
  {
    return InterlockedIncrement (&refcount_);
  }

  STDMETHODIMP_(ULONG) Release()
  {
    ULONG refcount = InterlockedDecrement (&refcount_);

    if (refcount == 0 && !lockcount_)
      delete this;

    return refcount;
  }

  STDMETHODIMP CreateInstance (IUnknown * outher, REFIID riid, void ** object)
  {
    /* FIXME: Should be consider COM aggregation? */
    IAsioDriver *driver = nullptr;
    ASIO_PROXY_DEBUG ("Creating instance");
    auto hr = IAsioDriver::CreateInstance (&driver);
    if (FAILED (hr)) {
      ASIO_PROXY_ERROR ("Couldn't create instance");
      return hr;
    }

    *object = driver;
    return S_OK;
  }

  STDMETHODIMP LockServer (BOOL lock)
  {
    if (lock)
      InterlockedIncrement (&lockcount_);
    else
      InterlockedDecrement (&lockcount_);

    return S_OK;
  }

  static BOOL IsLocked ()
  {
    return lockcount_ > 0;
  }

  ULONG refcount_ = 1;
  static ULONG lockcount_;
};

ULONG IAsioClassFactory::lockcount_ = 0;

BOOL WINAPI
DllMain (HINSTANCE module, DWORD reason, LPVOID reserved)
{
  if (reason == DLL_PROCESS_ATTACH)
    asio_proxy_logger_init ();

  return TRUE;
}

STDAPI
DllRegisterServer ()
{
  auto ret = RegisterDriver (IID_ASIO_DRIVER, DLL_NAME,
      DRIVER_NAME, DRIVER_DESC, DRIVER_APARTMENT);

  if (!ret)
    return E_FAIL;

  return S_OK;
}

STDAPI
DllUnregisterServer ()
{
  UnregisterDriver (IID_ASIO_DRIVER, DRIVER_NAME);

  return S_OK;
}

STDAPI
DllGetClassObject (REFCLSID clsid, REFIID riid, void ** object)
{
  ASIO_PROXY_DEBUG ("CLSID "
      "{ %08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX }",
  clsid.Data1, clsid.Data2, clsid.Data3,
  clsid.Data4[0], clsid.Data4[1], clsid.Data4[2], clsid.Data4[3],
  clsid.Data4[4], clsid.Data4[5], clsid.Data4[6], clsid.Data4[7]);

  ASIO_PROXY_DEBUG ("IID "
      "{ %08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX }",
        riid.Data1, riid.Data2, riid.Data3,
        riid.Data4[0], riid.Data4[1], riid.Data4[2], riid.Data4[3],
        riid.Data4[4], riid.Data4[5], riid.Data4[6], riid.Data4[7]);

  if (riid != IID_IUnknown && riid != IID_IClassFactory) {
    ASIO_PROXY_WARNING ("Unexpected factory interface id");
    return E_NOINTERFACE;
  }

  if (clsid != IID_ASIO_DRIVER) {
    ASIO_PROXY_WARNING ("Unexpected interface id");
    return E_NOINTERFACE;
  }

  *object = new IAsioClassFactory ();

  return S_OK;
}

STDAPI
DllCanUnloadNow ()
{
  if (IAsioClassFactory::IsLocked ()) {
    ASIO_PROXY_DEBUG ("Cannot unload now");
    return S_FALSE;
  }

  ASIO_PROXY_DEBUG ("Can unload now");

  return S_OK;
}