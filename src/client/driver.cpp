#pragma once

#include "driver.h"
#include "logger.h"
#include <string.h>

/* IUnknown */
STDMETHODIMP IAsioDriver::QueryInterface (REFIID riid, void ** object)
{
  if (riid == IID_IUnknown) {
    *object = static_cast<IUnknown*> (static_cast<IAsioDriver *> (this));
    AddRef ();
    return S_OK;
  } else if (riid == IID_ASIO_DRIVER) {
    *object = this;
    AddRef ();
    return S_OK;
  }

  *object = nullptr;
  return E_NOINTERFACE;
}

STDMETHODIMP_ (ULONG) IAsioDriver::AddRef ()
{
  return InterlockedIncrement (&refcount_);
}

STDMETHODIMP_ (ULONG) IAsioDriver::Release ()
{
  ULONG refcount = InterlockedDecrement (&refcount_);

  if (refcount == 0)
    delete this;

  return refcount;
}

/* IAsio */
ASIOBool IAsioDriver::init (void *handle)
{
  ASIO_PROXY_DEBUG ("INIT");
  return ASIOTrue;
}

void IAsioDriver::getDriverName (char * name)
{
  strcpy (name, "GStreamer ASIO Proxy Driver");
}

long IAsioDriver::getDriverVersion ()
{
  return 0x00000001L;
}

void IAsioDriver::getErrorMessage (char * string)
{
  strcpy (string, error_msg_);
}

ASIOError IAsioDriver::start ()
{
  ASIO_PROXY_DEBUG ("start");
  return asio_proxy_client_start (client_);
}

ASIOError IAsioDriver::stop ()
{
  ASIO_PROXY_DEBUG ("stop");
  return asio_proxy_client_stop (client_);
}

ASIOError IAsioDriver::getChannels (long * numInputChannels,
    long * numOutputChannels)
{
  ASIO_PROXY_DEBUG ("get channels");
  return asio_proxy_client_get_channels (client_, numInputChannels,
      numOutputChannels);
}

ASIOError IAsioDriver::getLatencies (long *inputLatency, long *outputLatency)
{
  ASIO_PROXY_DEBUG ("get latency");
  if (inputLatency)
    *inputLatency = kBlockSize;
  if (outputLatency)
    *outputLatency = 2 * kBlockSize;

  return ASE_OK;
}

ASIOError IAsioDriver::getBufferSize (long *minSize, long *maxSize,
    long *preferredSize, long *granularity)
{
  ASIO_PROXY_DEBUG ("get buffer size");
  if (minSize)
    *minSize = kBlockSize;
  if (maxSize)
    *maxSize = kBlockSize;
  if (preferredSize)
    *preferredSize = kBlockSize;
  if (granularity)
    *granularity = 0;

  return ASE_OK;
}

ASIOError IAsioDriver::canSampleRate (ASIOSampleRate sampleRate)
{
  ASIO_PROXY_DEBUG ("can samplerate %d", sampleRate);

  if (sampleRate != 48000)
    return ASE_NoClock;

  return ASE_OK;
}

ASIOError IAsioDriver::getSampleRate (ASIOSampleRate *sampleRate)
{
  ASIO_PROXY_DEBUG ("get samplerate");

  if (sampleRate)
    *sampleRate = 48000;

  return ASE_OK;
}

ASIOError IAsioDriver::setSampleRate (ASIOSampleRate sampleRate)
{
  ASIO_PROXY_DEBUG ("set samplerate %d", sampleRate);
  if (sampleRate != 48000)
    return ASE_NoClock;
  return ASE_OK;
}

ASIOError IAsioDriver::getClockSources (ASIOClockSource *clocks,
    long *numSources)
{
  ASIO_PROXY_DEBUG ("get clock sources");

  if (!clocks || !numSources || *numSources <= 0) {
    ASIO_PROXY_WARNING ("Invalid param");
    return ASE_InvalidParameter;
  }

  clocks[0].index = 0;
  clocks[0].associated_channel = -1;
  clocks[0].associated_group = -1;
  clocks[0].isCurrentSource = ASIOTrue;
  strcpy(clocks[0].name, "Internal");
  *numSources = 1;

  return ASE_OK;
}

ASIOError IAsioDriver::setClockSource (long reference)
{
  ASIO_PROXY_DEBUG ("set clock source %d", reference);
  if (reference != 0)
    return ASE_NotPresent;

  return ASE_OK;
}

ASIOError IAsioDriver::getSamplePosition (ASIOSamples *sPos,
    ASIOTimeStamp *tStamp)
{
  return asio_proxy_client_get_sample_position (client_, sPos, tStamp);
}

ASIOError IAsioDriver::getChannelInfo (ASIOChannelInfo *info)
{
  ASIO_PROXY_DEBUG ("get channel info");;

  return asio_proxy_client_get_channel_info (client_, info);
}

ASIOError IAsioDriver::createBuffers (ASIOBufferInfo *bufferInfos,
    long numChannels, long bufferSize, ASIOCallbacks *callbacks)
{
  ASIO_PROXY_DEBUG ("create");
  return asio_proxy_client_create_bufffer (client_, bufferInfos,
      numChannels, bufferSize, callbacks);
}

ASIOError IAsioDriver::disposeBuffers ()
{
  ASIO_PROXY_DEBUG ("dispose");
  return asio_proxy_client_dispose_bufffer (client_);
}

ASIOError IAsioDriver::controlPanel ()
{
  return ASE_NotPresent;
}

ASIOError IAsioDriver::future (long selector,void *opt)
{
  return ASE_NotPresent;
}

ASIOError IAsioDriver::outputReady ()
{
  return ASE_NotPresent;
}

HRESULT IAsioDriver::CreateInstance (IAsioDriver ** driver)
{
  auto client = asio_proxy_client_new (L"\\\\.\\pipe\\gst.asio.proxy");
  if (!client)
    return E_FAIL;

  auto self = new IAsioDriver ();
  self->client_ = client;

  *driver = self;

  return S_OK;
}

IAsioDriver::~IAsioDriver ()
{
  ASIO_PROXY_DEBUG ("FIN");
  if (client_)
    asio_proxy_client_free (client_);
}