#pragma once

#include <windows.h>
#include "asio.h"

static const UINT kBlockSize = 512;
static const long kMaxChannels = 16;

struct AsioProxyClient;

AsioProxyClient * asio_proxy_client_new (const wchar_t * address);

void      asio_proxy_client_free (AsioProxyClient * client);

ASIOError asio_proxy_client_create_bufffer (AsioProxyClient * client,
                                            ASIOBufferInfo * infos,
                                            long num_channels,
                                            long buffer_size,
                                            ASIOCallbacks * callbacks);

ASIOError asio_proxy_client_dispose_bufffer (AsioProxyClient * client);

ASIOError asio_proxy_client_start (AsioProxyClient * client);

ASIOError asio_proxy_client_stop (AsioProxyClient * client);

ASIOError asio_proxy_client_get_sample_position (AsioProxyClient * client,
                                                 ASIOSamples * pos,
                                                 ASIOTimeStamp * timestamp);

ASIOError asio_proxy_client_get_channel_info (AsioProxyClient * client,
                                              ASIOChannelInfo * info);

ASIOError asio_proxy_client_get_channels (AsioProxyClient * client,
                                          long * in_ch,
                                          long * out_ch);