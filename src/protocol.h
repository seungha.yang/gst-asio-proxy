#pragma once

#include <windows.h>

enum class AsioProxyPktType : UINT64
{
  PktTypeNone = 0,
  PktTypeStart = (1 << 0),
  PktTypeStop = (1 << 1),
  PktTypeSwitch = (1 << 2),
};

DEFINE_ENUM_FLAG_OPERATORS (AsioProxyPktType);

#pragma pack(1)

struct MMFData
{
  DWORD pid;
  HANDLE mmf_handle;
  SIZE_T mmf_size;
  DWORD num_channels;
  DWORD asio_format;
};

struct ClientMsg
{
  AsioProxyPktType type;
  UINT64 payload;
};

#pragma pack(pop)