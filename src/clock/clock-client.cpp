#include "clock-client.h"
#include <windows.h>
#include <avrt.h>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <atomic>

GST_DEBUG_CATEGORY_STATIC (asio_proxy_clock_client_debug);
#define GST_CAT_DEFAULT asio_proxy_clock_client_debug

struct ClientConn : public OVERLAPPED
{
  ClientConn (AsioProxyClockClient * c, HANDLE pipe_handle)
    : client (c), pipe (pipe_handle)
  {
    OVERLAPPED *p = static_cast<OVERLAPPED *> (this);
    p->Internal = 0;
    p->InternalHigh = 0;
    p->Offset = 0;
    p->OffsetHigh = 0;
  }

  AsioProxyClockClient *client;
  HANDLE pipe;
  UINT8 clock_type = 0;
  UINT64 client_msg;
  UINT64 server_msg[2];
};

struct AsioClockClientPrivate
{
  AsioClockClientPrivate ()
  {
    clock = (GstClock *) g_object_new (GST_TYPE_SYSTEM_CLOCK, nullptr);
    gst_object_ref_sink (clock);
    cancellable = CreateEvent (nullptr, TRUE, FALSE, nullptr);
    timer_handle = CreateWaitableTimerW (nullptr, FALSE, nullptr);
    QueryPerformanceFrequency (&freq);
  }

  ~AsioClockClientPrivate ()
  {
    SetEvent (cancellable);

    g_clear_pointer (&loop_thread, g_thread_join);

    CloseHandle (cancellable);
    CloseHandle (timer_handle);
    gst_clear_object (&clock);
    g_free (address);
  }

  GThread *loop_thread = nullptr;
  HANDLE cancellable;
  HANDLE timer_handle;
  wchar_t *address = nullptr;
  LARGE_INTEGER freq;
  GstClock *clock;
  std::shared_ptr<ClientConn> conn;
  bool is_busy = false;
  bool need_sync = false;
  bool is_qpc = false;
  std::atomic<bool> aborted = { false };
  GstClockTime clock_time = GST_CLOCK_TIME_NONE;
  UINT64 synced_time;
  std::mutex lock;
  std::condition_variable cond;
};

struct _AsioProxyClockClient
{
  GstObject parent;

  AsioClockClientPrivate *priv;
};

static void do_sync (AsioProxyClockClient * self);

static void asio_proxy_clock_client_finalize (GObject * object);

#define asio_proxy_clock_client_parent_class parent_class
G_DEFINE_TYPE (AsioProxyClockClient, asio_proxy_clock_client, GST_TYPE_OBJECT);

static void
asio_proxy_clock_client_class_init (AsioProxyClockClientClass * klass)
{
  auto object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = asio_proxy_clock_client_finalize;

  GST_DEBUG_CATEGORY_INIT (asio_proxy_clock_client_debug,
      "asio-proxy-clock-client", 0, "asio-proxy-clock-client");
}

static void
asio_proxy_clock_client_init (AsioProxyClockClient * self)
{
  self->priv = new AsioClockClientPrivate ();
}

static void
asio_proxy_clock_client_finalize (GObject * object)
{
  auto self = ASIO_PROXY_CLOCK_CLIENT (object);

  delete self->priv;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void WINAPI
read_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ClientConn *) overlap;
  auto self = conn->client;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    SetEvent (priv->cancellable);
    return;
  }

  {
    std::lock_guard <std::mutex> lk (priv->lock);
    priv->synced_time = conn->server_msg[0];
    priv->clock_time = conn->server_msg[1];
    priv->cond.notify_all ();
  }

  priv->is_busy = false;
  if (priv->need_sync)
    do_sync (self);
}

static void WINAPI
write_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ClientConn *) overlap;
  auto self = conn->client;
  auto priv = self->priv;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    SetEvent (priv->cancellable);
    return;
  }

  if (!ReadFileEx (conn->pipe, &conn->server_msg, sizeof (conn->server_msg),
      conn, read_finished)) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    SetEvent (priv->cancellable);
  }
}

static void
do_sync (AsioProxyClockClient * self)
{
  auto priv = self->priv;
  auto conn = priv->conn;
  LARGE_INTEGER now;

  priv->need_sync = false;
  priv->is_busy = true;

  QueryPerformanceCounter (&now);
  conn->client_msg = now.QuadPart;

  if (!WriteFileEx (conn->pipe, &conn->client_msg, sizeof (conn->client_msg),
      conn.get (), write_finished)) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    SetEvent (priv->cancellable);
  }
}

static void WINAPI
conn_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ClientConn *) overlap;
  auto self = conn->client;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    SetEvent (priv->cancellable);
    return;
  }

  if (conn->clock_type == 1) {
    GST_DEBUG_OBJECT (self, "Server clock is system clock");
    std::lock_guard <std::mutex> lk (priv->lock);
    priv->is_qpc = true;
    priv->clock_time = gst_clock_get_internal_time (priv->clock);
    priv->cond.notify_all ();
  } else {
    LARGE_INTEGER timer_start;
    timer_start.QuadPart = -10000000LL;
    SetWaitableTimer (priv->timer_handle, &timer_start, 500, nullptr, nullptr,
          FALSE);

    GST_DEBUG_OBJECT (self, "Server clock is not system clock, run timer");
    do_sync (self);
  }
}

#define PRINT_LAST_ERR(func) G_STMT_START { \
  auto last_err_ = GetLastError (); \
  auto err_ = g_win32_error_message ((gint) last_err_); \
  GST_ERROR (#func " failed with 0x%x (%s)", last_err_, err_); \
  g_free (err_); \
} G_STMT_END

static gpointer
clock_client_thread_func (AsioProxyClockClient * self)
{
  auto priv = self->priv;
  HANDLE pipe;
  HANDLE waitables[2];
  DWORD task_idx = 0;
  auto task_handle = AvSetMmThreadCharacteristicsA ("Pro Audio", &task_idx);
  DWORD mode = PIPE_READMODE_MESSAGE;

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
  CREATEFILE2_EXTENDED_PARAMETERS params;
  memset (&params, 0, sizeof (CREATEFILE2_EXTENDED_PARAMETERS));
  params.dwSize = sizeof (CREATEFILE2_EXTENDED_PARAMETERS);
  params.dwFileAttributes = 0;
  params.dwFileFlags = FILE_FLAG_OVERLAPPED;
  params.dwSecurityQosFlags = SECURITY_IMPERSONATION;
#endif

#if (_WIN32_WINNT >= _WIN32_WINNT_WIN8)
  pipe = CreateFile2 (priv->address, GENERIC_READ | GENERIC_WRITE, 0,
        OPEN_EXISTING, &params);
#else
  pipe = CreateFileW (priv->address,
        GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED, nullptr);
#endif

  if (pipe == INVALID_HANDLE_VALUE)
    goto out;

  if (!SetNamedPipeHandleState (pipe, &mode, nullptr, nullptr)) {
    PRINT_LAST_ERR (SetNamedPipeHandleState);
    goto out;
  }

  priv->conn = std::make_shared<ClientConn> (self, pipe);
  priv->is_busy = true;
  if (!ReadFileEx (pipe, &priv->conn->clock_type, sizeof (priv->conn->clock_type),
      priv->conn.get (), conn_finished)) {
    goto out;
  }

  waitables[0] = priv->timer_handle;
  waitables[1] = priv->cancellable;

  do {
    auto ret = WaitForMultipleObjectsEx (G_N_ELEMENTS (waitables), waitables,
        FALSE, INFINITE, TRUE);

    switch (ret) {
      case WAIT_OBJECT_0:
      {
        priv->need_sync = true;
        if (!priv->is_busy)
          do_sync (self);
        break;
      }
      case WAIT_IO_COMPLETION:
        break;
      default:
        goto out;
    }
  } while (true);

out:
  if (pipe != INVALID_HANDLE_VALUE) {
    CancelIo (pipe);
    CloseHandle (pipe);
  }

  priv->conn = nullptr;

  if (task_handle)
    AvRevertMmThreadCharacteristics (task_handle);

  {
    std::lock_guard <std::mutex> lk (priv->lock);
    priv->aborted = true;
    priv->cond.notify_all ();
  }

  return nullptr;
}

AsioProxyClockClient *
asio_proxy_clock_client_new (const gchar * address)
{
  g_return_val_if_fail (address, nullptr);

  auto self = (AsioProxyClockClient *)
      g_object_new (ASIO_PROXY_TYPE_CLOCK_CLIENT, nullptr);
  gst_object_ref_sink (self);

  auto priv = self->priv;
  priv->address = (wchar_t *)
      g_utf8_to_utf16 (address, -1, nullptr, nullptr, nullptr);;
  priv->loop_thread = g_thread_new ("AsioProxyClockClient",
      (GThreadFunc) clock_client_thread_func, self);

  {
    std::unique_lock <std::mutex> lk (priv->lock);
    while (!priv->aborted && !GST_CLOCK_TIME_IS_VALID (priv->clock_time))
      priv->cond.wait (lk);
  }

  if (priv->aborted)
    gst_clear_object (&self);

  return self;
}

GstClockTime
asio_proxy_clock_get_time (AsioProxyClockClient * client)
{
  auto priv = client->priv;

  if (priv->is_qpc)
    return gst_clock_get_internal_time (priv->clock);

  UINT64 synced_time;
  GstClockTime master_time;
  {
    std::lock_guard <std::mutex> lk (priv->lock);
    synced_time = priv->synced_time;
    master_time = priv->clock_time;
  }

  LARGE_INTEGER now;
  QueryPerformanceCounter (&now);
  auto diff = gst_util_uint64_scale (now.QuadPart - synced_time, GST_SECOND,
      priv->freq.QuadPart);

  return master_time + diff;
}
