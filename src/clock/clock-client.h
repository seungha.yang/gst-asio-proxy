#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

#define ASIO_PROXY_TYPE_CLOCK_CLIENT (asio_proxy_clock_client_get_type())
G_DECLARE_FINAL_TYPE (AsioProxyClockClient, asio_proxy_clock_client,
    ASIO_PROXY, CLOCK_CLIENT, GstObject);

AsioProxyClockClient * asio_proxy_clock_client_new (const gchar * address);

GstClockTime           asio_proxy_clock_get_time   (AsioProxyClockClient * client);

G_END_DECLS