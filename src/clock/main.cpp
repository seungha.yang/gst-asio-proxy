#include <windows.h>
#include <gst/gst.h>
#include "asioproxyclock.h"
#include "clock-client.h"

int
main (int argc, char *argv[])
{
  UINT mm_period = 0;
  TIMECAPS time_caps;

  gst_init (nullptr, nullptr);

  if (timeGetDevCaps (&time_caps, sizeof (time_caps)) == TIMERR_NOERROR) {
    auto res = MIN (MAX (time_caps.wPeriodMin, 1), time_caps.wPeriodMax);
    if (timeBeginPeriod (res) == TIMERR_NOERROR)
      mm_period = res;
  }

  auto clock = gst_system_clock_obtain ();
  auto asio_clock = asio_proxy_clock_new ("\\\\.\\pipe\\gst.asio.proxy.clock");

  while (true) {
    GstClockTime sys_now, asio_now;
    GstClockTimeDiff diff;
    sys_now = gst_clock_get_time (clock);
    asio_now = gst_clock_get_time (asio_clock);

    diff = GST_CLOCK_DIFF (sys_now, asio_now);
    gst_println ("SYS: %" GST_TIME_FORMAT ", ASIO: %" GST_TIME_FORMAT
        ", DIFF %" GST_STIME_FORMAT, GST_TIME_ARGS (sys_now),
        GST_TIME_ARGS (asio_now), GST_STIME_ARGS (diff));

    Sleep (1000);

  };

  if (mm_period)
    timeEndPeriod (mm_period);

  return 0;
}
