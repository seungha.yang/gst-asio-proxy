#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

#define ASIO_PROXY_TYPE_CLOCK (asio_proxy_clock_get_type())
G_DECLARE_FINAL_TYPE (AsioProxyClock, asio_proxy_clock,
    ASIO_PROXY, CLOCK, GstSystemClock);

GstClock * asio_proxy_clock_new (const gchar * address);

G_END_DECLS