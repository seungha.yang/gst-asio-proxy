#include "asioproxyclock.h"
#include "clock-client.h"

GST_DEBUG_CATEGORY_STATIC (asio_proxy_clock_debug);
#define GST_CAT_DEFAULT asio_proxy_clock_debug

struct _AsioProxyClock
{
  GstSystemClock parent;

  AsioProxyClockClient *client;
};

static void asio_proxy_clock_finalize (GObject * object);
static GstClockTime asio_proxy_clock_get_internal_time (GstClock * clock);

#define asio_proxy_clock_parent_class parent_class
G_DEFINE_TYPE (AsioProxyClock, asio_proxy_clock, GST_TYPE_SYSTEM_CLOCK);

static void
asio_proxy_clock_class_init (AsioProxyClockClass * klass)
{
  auto object_class = G_OBJECT_CLASS (klass);
  auto clock_class = GST_CLOCK_CLASS (klass);

  object_class->finalize = asio_proxy_clock_finalize;
  clock_class->get_internal_time = asio_proxy_clock_get_internal_time;

 GST_DEBUG_CATEGORY_INIT (asio_proxy_clock_debug, "asio-proxy-clock", 0,
      "asio-proxy-clock");
}

static void
asio_proxy_clock_init (AsioProxyClock * self)
{
}

static void
asio_proxy_clock_finalize (GObject * object)
{
  auto self = ASIO_PROXY_CLOCK (object);

  gst_clear_object (&self->client);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GstClockTime
asio_proxy_clock_get_internal_time (GstClock * clock)
{
  auto self = ASIO_PROXY_CLOCK (clock);

  return asio_proxy_clock_get_time (self->client);
}

GstClock *
asio_proxy_clock_new (const gchar * address)
{
  auto client = asio_proxy_clock_client_new (address);
  if (!client)
    return nullptr;

  auto self = (AsioProxyClock *) g_object_new (ASIO_PROXY_TYPE_CLOCK, nullptr);
  self->client = client;

  return (GstClock *) self;
}
