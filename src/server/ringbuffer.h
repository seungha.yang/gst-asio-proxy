#pragma once

#include <gst/audio/audio.h>

G_BEGIN_DECLS

typedef struct _AsioProxyRingbuffer AsioProxyRingbuffer;

AsioProxyRingbuffer * asio_proxy_ringbuffer_new (const GstAudioInfo * info,
                                                 guint buffer_size,
                                                 guint latency,
                                                 guint num_channels);

void    asio_proxy_ringbuffer_free (AsioProxyRingbuffer * rb);

guint64 asio_proxy_ringbuffer_advance (AsioProxyRingbuffer * rb);

guint   asio_proxy_ringbuffer_get_size (AsioProxyRingbuffer * rb);

void    asio_proxy_ringbuffer_write    (AsioProxyRingbuffer * rb,
                                        guint64 seq,
                                        guint channel,
                                        void * in);

void    asio_proxy_ringbuffer_read     (AsioProxyRingbuffer * rb,
                                        guint64 seq,
                                        guint channel,
                                        void * out);

G_END_DECLS