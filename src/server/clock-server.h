#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

#define ASIO_PROXY_TYPE_CLOCK_SERVER (asio_proxy_clock_server_get_type())
G_DECLARE_FINAL_TYPE (AsioProxyClockServer, asio_proxy_clock_server,
    ASIO_PROXY, CLOCK_SERVER, GstObject);

AsioProxyClockServer * asio_proxy_clock_server_new (const gchar * address,
                                                    GstClock * clock);

G_END_DECLS