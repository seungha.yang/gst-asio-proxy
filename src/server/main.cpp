#include <windows.h>
#include <gst/gst.h>
#include <gst/net/gstptpclock.h>
#include <gst/audio/audio.h>
#include "clock-server.h"
#include "proxy-server.h"

int
main (int argc, char *argv_[])
{
  UINT mm_period = 0;
  TIMECAPS time_caps;
  guint ttl = G_MAXUINT;
  guint domain = G_MAXUINT;
  gint num_channels = 2;
  gchar **interfaces = nullptr;
  GError *err = nullptr;
  gchar *format = nullptr;
  GstAudioFormat audio_format = GST_AUDIO_FORMAT_UNKNOWN;
  GOptionEntry options[] = {
    {"domain", 'd', 0, G_OPTION_ARG_INT, &domain,
        "PTP domain, if unspecified, system clock will be used", nullptr },
    {"interfaces", 'i', 0, G_OPTION_ARG_STRING_ARRAY, &interfaces,
        "(optional) interface names to listen on for PTP packets", nullptr },
    {"ttl", 't', 0, G_OPTION_ARG_INT, &ttl, "(optional) TTL to use for multicast packets",
        nullptr },
    {"channels", 'c', 0, G_OPTION_ARG_INT, &num_channels,
        "The number of audio channels, valid range is [1, 16] and default is 2", nullptr },
    {"format", 'f', 0, G_OPTION_ARG_STRING, &format,
        "Audio format string (e.g., \"S24LE\", \"F32LE\")", nullptr },
    {nullptr, }
  };
  gchar **argv = g_win32_get_command_line ();
  GOptionContext *opt_ctx =
      g_option_context_new ("GStreamer ASIO proxy server");

  g_option_context_add_main_entries (opt_ctx, options, nullptr);
  g_option_context_add_group (opt_ctx, gst_init_get_option_group ());
  if (!g_option_context_parse_strv (opt_ctx, &argv, &err)) {
    g_error ("Error parsing options: %s", err->message);
    g_clear_error (&err);
    return 1;
  }
  g_option_context_free (opt_ctx);

  if (num_channels <= 0 || ASIO_PROXY_MAX_CHANNELS > 16) {
    g_error ("Invalid channels %d, valid range is [1, %d]",
        num_channels, ASIO_PROXY_MAX_CHANNELS);
    return 1;
  }

  if (format) {
    if (g_strcasecmp (format, "S24LE") == 0)
      audio_format = GST_AUDIO_FORMAT_S24LE;
    else if (g_strcasecmp (format, "F32LE") == 0)
      audio_format = GST_AUDIO_FORMAT_F32LE;
    else
      gst_printerrln ("Not supported format string %s", format);
  }

  if (audio_format == GST_AUDIO_FORMAT_UNKNOWN)
    audio_format = GST_AUDIO_FORMAT_F32LE;

  gst_println ("Selected audio format %s",
      gst_audio_format_to_string (audio_format));

  if (timeGetDevCaps (&time_caps, sizeof (time_caps)) == TIMERR_NOERROR) {
    auto res = MIN (MAX (time_caps.wPeriodMin, 1), time_caps.wPeriodMax);
    if (timeBeginPeriod (res) == TIMERR_NOERROR)
      mm_period = res;
  }

  GstClock *clock;
  if (domain >= 0 && domain <= G_MAXUINT8) {
    auto config = gst_structure_new_empty ("ptp-config");
    if (interfaces && g_strv_length (interfaces) > 0) {
      gst_structure_set (config,
          "interfaces", G_TYPE_STRV, interfaces, nullptr);
    }

    if (ttl != G_MAXUINT)
      gst_structure_set (config, "ttl", G_TYPE_UINT, ttl, nullptr);

    if (!gst_ptp_init_full (config)) {
      g_error ("Couldn't initialize ptp module");
      return 1;
    }

    clock = gst_ptp_clock_new ("asio-server-clock", domain);
    gst_clock_wait_for_sync (clock, GST_CLOCK_TIME_NONE);

    gst_println ("Using PTP clock");
  } else {
    clock = gst_system_clock_obtain ();
    gst_println ("Using system clock");
  }

  auto clock_server =
      asio_proxy_clock_server_new ("\\\\.\\pipe\\gst.asio.proxy.clock", clock);
  auto proxy_server =
      asio_proxy_server_new ("\\\\.\\pipe\\gst.asio.proxy", clock,
      (guint) num_channels, audio_format);

  while (true) {
    Sleep (1000);
  };

  if (mm_period)
    timeEndPeriod (mm_period);

  return 0;
}
