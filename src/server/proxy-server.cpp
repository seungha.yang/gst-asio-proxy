#include "proxy-server.h"
#include "ringbuffer.h"
#include <windows.h>
#include <avrt.h>
#include <vector>
#include <memory>
#include <unordered_map>
#include <mutex>
#include <atomic>
#include <queue>
#include "../protocol.h"

GST_DEBUG_CATEGORY_STATIC (asio_proxy_server_debug);
#define GST_CAT_DEFAULT asio_proxy_server_debug

#define PRINT_LAST_ERR(func) G_STMT_START { \
  auto last_err_ = GetLastError (); \
  auto err_ = g_win32_error_message ((gint) last_err_); \
  GST_ERROR (#func " failed with 0x%x (%s)", last_err_, err_); \
  g_free (err_); \
} G_STMT_END

struct ChannelData
{
  void *in_buffer[2];
  void *out_buffer[2];
};

struct ServerConn : public OVERLAPPED
{
  ServerConn (AsioProxyServer * s, HANDLE pipe_handle, UINT64 conn_id)
    : server (s), pipe (pipe_handle), id (conn_id)
  {
    OVERLAPPED *p = static_cast<OVERLAPPED *> (this);
    p->Internal = 0;
    p->InternalHigh = 0;
    p->Offset = 0;
    p->OffsetHigh = 0;
  }

  ~ServerConn ()
  {
    if (pipe != INVALID_HANDLE_VALUE) {
      CancelIo (pipe);
      DisconnectNamedPipe (pipe);
      CloseHandle (pipe);
    }

    if (mapped_buffer)
      UnmapViewOfFile (mapped_buffer);

    if (mmf_data.mmf_handle)
      CloseHandle (mmf_data.mmf_handle);
  }

  bool CreateSharedMemory (DWORD pid, SIZE_T size, UINT num_channels,
      DWORD asio_format)
  {
    mmf_data.pid = pid;
    mmf_data.mmf_handle = CreateFileMappingW (INVALID_HANDLE_VALUE,
        nullptr, PAGE_READWRITE | SEC_COMMIT, 0, size, nullptr);
    if (!mmf_data.mmf_handle) {
      PRINT_LAST_ERR (CreateFileMappingW);
      return false;
    }

    mmf_data.mmf_size = size;
    mmf_data.num_channels = num_channels;
    mmf_data.asio_format = asio_format;

    mapped_buffer = MapViewOfFile (mmf_data.mmf_handle, FILE_MAP_ALL_ACCESS,
        0, 0, size);
    if (!mapped_buffer) {
      PRINT_LAST_ERR (MapViewOfFile);
      return false;
    }

    auto ch_buf_size = size / (num_channels * 4);
    auto data = (UINT8 *) mapped_buffer;
    for (guint i = 0; i < num_channels; i++) {
      buffers[i].in_buffer[0] = data;
      data += ch_buf_size;
      buffers[i].in_buffer[1] = data;
      data += ch_buf_size;

      buffers[i].out_buffer[0] = data;
      data += ch_buf_size;
      buffers[i].out_buffer[1] = data;
      data += ch_buf_size;
    }

    return true;
  }

  AsioProxyServer *server;
  HANDLE pipe;
  UINT64 id;
  MMFData mmf_data = { };
  ClientMsg client_msg;
  UINT64 server_msg;
  void *mapped_buffer = nullptr;
  ChannelData buffers[ASIO_PROXY_MAX_CHANNELS] = { };
  bool running = false;
  bool busy = false;
  std::queue<UINT64> clock_msg;
};

struct AsioProxyServerPrivate
{
  AsioProxyServerPrivate ()
  {
    pid = GetCurrentProcessId ();
    cancellable = CreateEvent (nullptr, TRUE, FALSE, nullptr);
    QueryPerformanceFrequency (&freq);
  }

  ~AsioProxyServerPrivate ()
  {
    SetEvent (cancellable);

    g_clear_pointer (&loop_thread, g_thread_join);

    CloseHandle (cancellable);
    gst_clear_object (&clock);
    g_free (address);

    if (rb)
      asio_proxy_ringbuffer_free (rb);
  }

  UINT64 next_conn_id = 0;
  std::mutex lock;
  std::unordered_map<UINT64, std::shared_ptr<ServerConn>> conn_map;
  GThread *loop_thread = nullptr;
  HANDLE cancellable;
  HANDLE loop_thread_handle;
  wchar_t *address = nullptr;
  LARGE_INTEGER freq;
  GstClock *clock = nullptr;
  AsioProxyRingbuffer *rb = nullptr;
  std::atomic <bool> shutdown = { false };
  DWORD pid;
  guint num_channels = 0;
  DWORD asio_format;
};

struct _AsioProxyServer
{
  GstObject parent;

  AsioProxyServerPrivate *priv;
};

static void WINAPI
read_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap);
static void send_clock_tick_msg (ServerConn * conn);

static void asio_proxy_server_finalize (GObject * object);

#define asio_proxy_server_parent_class parent_class
G_DEFINE_TYPE (AsioProxyServer, asio_proxy_server, GST_TYPE_OBJECT);

static void
asio_proxy_server_class_init (AsioProxyServerClass * klass)
{
  auto object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = asio_proxy_server_finalize;

  GST_DEBUG_CATEGORY_INIT (asio_proxy_server_debug,
      "asio-proxy-server", 0, "asio-proxy-server");
}

static void
asio_proxy_server_init (AsioProxyServer * self)
{
  self->priv = new AsioProxyServerPrivate ();
}

static void
asio_proxy_server_finalize (GObject * object)
{
  auto self = ASIO_PROXY_SERVER (object);

  delete self->priv;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
close_connection (AsioProxyServer * self, ServerConn * conn)
{
  auto priv = self->priv;

  GST_DEBUG_OBJECT (self, "Closing conn-id %" G_GUINT64_FORMAT, conn->id);

  priv->conn_map.erase (conn->id);
}

static void WINAPI
write_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    close_connection (self, conn);
    return;
  }

  conn->busy = false;
  if (conn->running)
    send_clock_tick_msg (conn);
}

static void
send_clock_tick_msg (ServerConn * conn)
{
  if (conn->clock_msg.empty ())
    return;

  conn->busy = true;
  conn->server_msg = conn->clock_msg.front ();
  conn->clock_msg.pop ();

  if (!WriteFileEx (conn->pipe, &conn->server_msg, sizeof (conn->server_msg),
      conn, write_finished)) {
    GST_WARNING ("ReadFileEx failed");
    close_connection (conn->server, conn);
    return;
  }
}

static void
enqueue_clock_tick (ServerConn * conn, UINT64 tick_idx)
{
  if (!conn->running)
    return;

  conn->clock_msg.push (tick_idx);
  if (!conn->busy)
    send_clock_tick_msg (conn);
}

static void WINAPI
read_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
    return;
  }

  GST_DEBUG_OBJECT (self, "Got client msg %d", conn->client_msg.type);

  if (conn->client_msg.type == AsioProxyPktType::PktTypeStart) {
    conn->running = true;
  } else if (conn->client_msg.type == AsioProxyPktType::PktTypeStop) {
    conn->running = false;
  } else if ((conn->client_msg.type & AsioProxyPktType::PktTypeSwitch) ==
      AsioProxyPktType::PktTypeSwitch) {
    auto idx = conn->client_msg.payload % 2;
    UINT64 mask = ((UINT64) conn->client_msg.type) >> 3;
    for (guint i = 0; i < priv->num_channels; i++) {
      if ((mask & 0x1) != 0) {
        asio_proxy_ringbuffer_read (priv->rb, conn->client_msg.payload,
            i, conn->buffers[i].in_buffer[idx]);
      }

      if ((mask & 0x2) != 0) {
        asio_proxy_ringbuffer_write (priv->rb, conn->client_msg.payload,
            i, conn->buffers[i].out_buffer[idx]);
      }

      mask = mask >> 2;
    }
  }

  if (!ReadFileEx (conn->pipe, &conn->client_msg, sizeof (conn->client_msg),
      conn, read_finished)) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
  }
}

static void WINAPI
conn_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    close_connection (self, conn);
    return;
  }

  if (!ReadFileEx (conn->pipe, &conn->client_msg, sizeof (conn->client_msg),
      conn, read_finished)) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
  }
}

static void
on_incoming_connection (AsioProxyServer * self, HANDLE pipe)
{
  auto priv = self->priv;

  auto conn_id = priv->next_conn_id;
  priv->next_conn_id++;

  /* double buffers for each in/out per channel */
  auto size = asio_proxy_ringbuffer_get_size (priv->rb) *
      priv->num_channels * 4;

  GST_DEBUG_OBJECT (self, "New connection id %" G_GUINT64_FORMAT
      ", alloc size %d", conn_id, size);

  auto conn = std::make_shared<ServerConn> (self, pipe, conn_id);
  if (!conn->CreateSharedMemory (priv->pid, size, priv->num_channels,
      priv->asio_format)) {
    GST_ERROR_OBJECT (self, "Couldn't allocate shared memory");
    return;
  }

  priv->conn_map.insert ({conn->id, conn});
  if (!WriteFileEx (conn->pipe, &conn->mmf_data, sizeof (conn->mmf_data),
      conn.get (), conn_finished)) {
    PRINT_LAST_ERR (WriteFileEx);
    close_connection (self, conn.get ());
  }
}

static HANDLE
create_pipe (AsioProxyServer * self, OVERLAPPED * overlap,
    bool &io_pending)
{
  auto priv = self->priv;
  HANDLE pipe = CreateNamedPipeW (priv->address,
      PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
      PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
      PIPE_UNLIMITED_INSTANCES, sizeof (UINT64), sizeof (UINT64) * 2, 5000,
      nullptr);

  if (pipe == INVALID_HANDLE_VALUE) {
    PRINT_LAST_ERR (CreateNamedPipeA);
    return INVALID_HANDLE_VALUE;
  }

  if (ConnectNamedPipe (pipe, overlap)) {
    PRINT_LAST_ERR (ConnectNamedPipe);
    return INVALID_HANDLE_VALUE;
  }

  io_pending = false;
  guint last_err = GetLastError ();

  switch (last_err) {
    case ERROR_IO_PENDING:
      io_pending = true;
      break;
    case ERROR_PIPE_CONNECTED:
      SetEvent (overlap->hEvent);
      break;
    default:
      PRINT_LAST_ERR (ConnectNamedPipe);
      CloseHandle (pipe);
      return INVALID_HANDLE_VALUE;
  }

  return pipe;
}

static void NTAPI
timer_tick_cb (ULONG_PTR param)
{
  auto self = (AsioProxyServer *) param;
  auto priv = self->priv;

  auto idx = asio_proxy_ringbuffer_advance (priv->rb);
  for (auto & it : priv->conn_map)
    enqueue_clock_tick (it.second.get (), idx);
}

static gpointer
proxy_server_clock_thread_func (AsioProxyServer * self)
{
  auto priv = self->priv;
  DWORD task_idx = 0;
  auto task_handle = AvSetMmThreadCharacteristicsA ("Pro Audio", &task_idx);
  GstClockTime start_time = gst_clock_get_time (priv->clock);
  guint64 count = 0;
  /* FIXME */
  guint64 buf_size = 512;
  guint64 rate = 48000;
  GstClockID clock_id = gst_clock_new_single_shot_id (priv->clock, start_time);

  do {
    GstClockTime target_time = gst_util_uint64_scale (GST_SECOND,
        buf_size * count, rate);
    count++;

    auto to_wait = start_time + target_time;

    gst_clock_single_shot_id_reinit (priv->clock, clock_id, to_wait);
    gst_clock_id_wait (clock_id, nullptr);

    if (!QueueUserAPC (timer_tick_cb,
        priv->loop_thread_handle, (ULONG_PTR) self)) {
      PRINT_LAST_ERR (QueueUserAPC);
      SetEvent (priv->cancellable);
      break;
    }
  } while (!priv->shutdown.load (std::memory_order_acquire));

  if (task_handle)
    AvRevertMmThreadCharacteristics (task_handle);

  return nullptr;
}

static gpointer
proxy_server_thread_func (AsioProxyServer * self)
{
  auto priv = self->priv;
  bool io_pending = false;
  HANDLE pipe;
  OVERLAPPED overlap = { };
  HANDLE waitables[2];
  DWORD task_idx = 0;
  auto task_handle = AvSetMmThreadCharacteristicsA ("Pro Audio", &task_idx);
  GThread *clock_thread = nullptr;
  auto cur_thread = GetCurrentThread ();

  overlap.hEvent = CreateEvent (nullptr, TRUE, TRUE, nullptr);
  pipe = create_pipe (self, &overlap, io_pending);
  if (pipe == INVALID_HANDLE_VALUE)
    goto out;

  waitables[0] = overlap.hEvent;
  waitables[1] = priv->cancellable;

  DuplicateHandle (GetCurrentProcess(), cur_thread,
      GetCurrentProcess(), &priv->loop_thread_handle, 0, FALSE,
      DUPLICATE_SAME_ACCESS);

  clock_thread = g_thread_new ("AsioProxyClock",
      (GThreadFunc) proxy_server_clock_thread_func, self);

  do {
    auto ret = WaitForMultipleObjectsEx (G_N_ELEMENTS (waitables), waitables,
        FALSE, INFINITE, TRUE);

    switch (ret) {
      case WAIT_OBJECT_0:
      {
        DWORD nbytes;

        if (io_pending
            && !GetOverlappedResult (pipe, &overlap, &nbytes, FALSE)) {
          PRINT_LAST_ERR (GetOverlappedResult);
          CloseHandle (pipe);
          pipe = INVALID_HANDLE_VALUE;
          break;
        }

        on_incoming_connection (self, pipe);

        pipe = create_pipe (self, &overlap, io_pending);
        break;
      }
      case WAIT_IO_COMPLETION:
        break;
      default:
        goto out;
    }
  } while (true);

out:
  priv->shutdown.store (true, std::memory_order_release);
  g_clear_pointer (&clock_thread, g_thread_join);

  if (pipe != INVALID_HANDLE_VALUE) {
    CancelIo (pipe);
    DisconnectNamedPipe (pipe);
    CloseHandle (pipe);
  }

  CloseHandle (overlap.hEvent);

  priv->conn_map.clear ();

  if (task_handle)
    AvRevertMmThreadCharacteristics (task_handle);

  if (priv->loop_thread_handle)
    CloseHandle (priv->loop_thread_handle);

  return nullptr;
}

AsioProxyServer *
asio_proxy_server_new (const gchar * address, GstClock * clock,
    guint num_channels, GstAudioFormat format)
{
  g_return_val_if_fail (address, nullptr);
  g_return_val_if_fail (GST_IS_CLOCK (clock), nullptr);
  g_return_val_if_fail (num_channels > 0, nullptr);
  g_return_val_if_fail (num_channels <= 16, nullptr);

  auto self = (AsioProxyServer *)
      g_object_new (ASIO_PROXY_TYPE_SERVER, nullptr);
  gst_object_ref_sink (self);

  GstAudioInfo info;
  gst_audio_info_set_format (&info, format, 48000, 1, nullptr);

  auto priv = self->priv;
  priv->rb = asio_proxy_ringbuffer_new (&info, 512, 2, num_channels);
  priv->clock = (GstClock *) gst_object_ref (clock);
  priv->address = (wchar_t *)
      g_utf8_to_utf16 (address, -1, nullptr, nullptr, nullptr);
  priv->num_channels = num_channels;
  switch (format) {
    case GST_AUDIO_FORMAT_F32LE:
      priv->asio_format = 19;
      break;
    case GST_AUDIO_FORMAT_S24LE:
      priv->asio_format = 17;
      break;
    default:
      g_assert_not_reached ();
      break;
  }
  priv->loop_thread = g_thread_new ("AsioProxyServer",
      (GThreadFunc) proxy_server_thread_func, self);

  return self;
}
