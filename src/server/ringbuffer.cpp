#include "ringbuffer.h"
#include "mixerorc.h"

typedef void (*WriteFunc) (AsioProxyRingbuffer * rb,
    guint64 seq, guint channel, void * in);

struct _AsioProxyRingbuffer
{
  GstAudioInfo info;

  guint seg_size;
  guint seg_total;
  guint8 *buffer[16] = { };
  guint64 buffer_idx;
  guint64 rw_offset;
  guint num_frames;
  guint num_channels;
  WriteFunc write_func;
};

static void
write_f32 (AsioProxyRingbuffer * rb, guint64 seq, guint channel, void * in)
{
  guint64 idx = (seq + rb->rw_offset) % rb->seg_total;
  audiomixer_orc_add_f32 ((float *) (rb->buffer[channel] + (idx * rb->seg_size)),
      (float *) in, rb->num_frames);
}

struct int24
{
  signed int data: 24;
};

static void
write_s24 (AsioProxyRingbuffer * rb, guint64 seq, guint channel, void * in)
{
  guint64 idx = (seq + rb->rw_offset) % rb->seg_total;
  guint remaining = rb->seg_size;
  guint8 *dst = rb->buffer[channel] + (idx * rb->seg_size);
  guint8 *src = (guint8 *) in;
  while (remaining >= 3) {
    int24 dst_val, src_val, rst_val;
    dst_val.data = GST_READ_UINT24_LE (dst);
    src_val.data = GST_READ_UINT24_LE (src);
    rst_val.data = src_val.data + dst_val.data;
    GST_WRITE_UINT24_LE (dst, rst_val.data);
    dst += 3;
    src += 3;
    remaining -= 3;
  }
}

AsioProxyRingbuffer *
asio_proxy_ringbuffer_new (const GstAudioInfo * info, guint buffer_size,
    guint latency, guint num_channels)
{
  g_return_val_if_fail (num_channels > 0, nullptr);
  g_return_val_if_fail (num_channels <= 16, nullptr);
  g_return_val_if_fail (GST_AUDIO_INFO_FORMAT (info) == GST_AUDIO_FORMAT_S24LE
      || GST_AUDIO_INFO_FORMAT (info) == GST_AUDIO_FORMAT_F32LE, nullptr);

  auto seg_size = buffer_size * GST_AUDIO_INFO_BPF (info);
  auto seg_total = 2 * (1 + latency);
  auto total_size = seg_total * seg_size;

  auto self = g_new0 (AsioProxyRingbuffer, 1);
  self->info = *info;
  self->buffer_idx = 0;
  self->rw_offset = latency + 1;
  self->seg_size = seg_size;
  self->seg_total = seg_total;
  self->num_frames = buffer_size;
  self->num_channels = num_channels;
  self->write_func = write_f32;
  if (GST_AUDIO_INFO_FORMAT (info) == GST_AUDIO_FORMAT_S24LE)
    self->write_func = write_s24;

  for (guint i = 0; i < num_channels; i++) {
    self->buffer[i] = (guint8 *) g_malloc (total_size);
    gst_audio_format_info_fill_silence (self->info.finfo,
        self->buffer[i], total_size);
  }

  return self;
}

void
asio_proxy_ringbuffer_free (AsioProxyRingbuffer * rb)
{
  if (!rb)
    return;

  for (guint i = 0; i < rb->num_channels; i++)
    g_free (rb->buffer[i]);
  g_free (rb);
}

guint64
asio_proxy_ringbuffer_advance (AsioProxyRingbuffer * rb)
{
  rb->buffer_idx++;

  guint64 idx = (rb->buffer_idx + rb->rw_offset) % rb->seg_total;
  for (guint i = 0; i < rb->num_channels; i++) {
    gst_audio_format_info_fill_silence (rb->info.finfo,
        rb->buffer[i] + (idx * rb->seg_size), rb->seg_size);
  }

  return rb->buffer_idx;
}

guint
asio_proxy_ringbuffer_get_size (AsioProxyRingbuffer * rb)
{
  return rb->seg_size;
}

void
asio_proxy_ringbuffer_write (AsioProxyRingbuffer * rb, guint64 seq,
    guint channel, void * in)
{
  rb->write_func (rb, seq, channel, in);
}

void
asio_proxy_ringbuffer_read (AsioProxyRingbuffer * rb, guint64 seq,
    guint channel, void * out)
{
  guint64 idx = seq % rb->seg_total;
  memcpy (out, rb->buffer[channel] + (idx * rb->seg_size), rb->seg_size);
}

