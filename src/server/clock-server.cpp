#include "clock-server.h"
#include <windows.h>
#include <avrt.h>
#include <vector>
#include <memory>
#include <unordered_map>

GST_DEBUG_CATEGORY_STATIC (asio_proxy_clock_server_debug);
#define GST_CAT_DEFAULT asio_proxy_clock_server_debug

struct ServerConn : public OVERLAPPED
{
  ServerConn (AsioProxyClockServer * s, HANDLE pipe_handle, UINT64 conn_id)
    : server (s), pipe (pipe_handle), id (conn_id)
  {
    OVERLAPPED *p = static_cast<OVERLAPPED *> (this);
    p->Internal = 0;
    p->InternalHigh = 0;
    p->Offset = 0;
    p->OffsetHigh = 0;
  }

  ~ServerConn ()
  {
    if (pipe != INVALID_HANDLE_VALUE) {
      CancelIo (pipe);
      DisconnectNamedPipe (pipe);
      CloseHandle (pipe);
    }
  }

  AsioProxyClockServer *server;
  HANDLE pipe;
  UINT64 id;
  UINT8 clock_type;
  UINT64 client_msg;
  UINT64 server_msg[2];
};

struct AsioClockServerPrivate
{
  AsioClockServerPrivate ()
  {
    cancellable = CreateEvent (nullptr, TRUE, FALSE, nullptr);
    QueryPerformanceFrequency (&freq);
  }

  ~AsioClockServerPrivate ()
  {
    SetEvent (cancellable);

    g_clear_pointer (&loop_thread, g_thread_join);

    CloseHandle (cancellable);
    gst_clear_object (&clock);
    g_free (address);
  }

  UINT64 next_conn_id = 0;
  std::unordered_map<UINT64, std::shared_ptr<ServerConn>> conn_map;
  GThread *loop_thread = nullptr;
  HANDLE cancellable;
  wchar_t *address = nullptr;
  LARGE_INTEGER freq;
  GstClock *clock = nullptr;
};

struct _AsioProxyClockServer
{
  GstObject parent;

  AsioClockServerPrivate *priv;
};

static void WINAPI
read_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap);

static void asio_proxy_clock_server_finalize (GObject * object);

#define asio_proxy_clock_server_parent_class parent_class
G_DEFINE_TYPE (AsioProxyClockServer, asio_proxy_clock_server, GST_TYPE_OBJECT);

static void
asio_proxy_clock_server_class_init (AsioProxyClockServerClass * klass)
{
  auto object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = asio_proxy_clock_server_finalize;

  GST_DEBUG_CATEGORY_INIT (asio_proxy_clock_server_debug,
      "asio-proxy-clock-server", 0, "asio-proxy-clock-server");
}

static void
asio_proxy_clock_server_init (AsioProxyClockServer * self)
{
  self->priv = new AsioClockServerPrivate ();
}

static void
asio_proxy_clock_server_finalize (GObject * object)
{
  auto self = ASIO_PROXY_CLOCK_SERVER (object);

  delete self->priv;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
close_connection (AsioProxyClockServer * self, ServerConn * conn)
{
  auto priv = self->priv;

  GST_DEBUG_OBJECT (self, "Closing conn-id %" G_GUINT64_FORMAT, conn->id);

  priv->conn_map.erase (conn->id);
}

static void WINAPI
write_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    close_connection (self, conn);
    return;
  }

  if (!ReadFileEx (conn->pipe, &conn->client_msg, sizeof (conn->client_msg),
      conn, read_finished)) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
  }
}

static void WINAPI
read_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
    return;
  }

  QueryPerformanceCounter (&now);
  auto gst_now = gst_clock_get_internal_time (priv->clock);
  if (now.QuadPart + priv->freq.QuadPart < conn->client_msg ||
      now.QuadPart > conn->client_msg + priv->freq.QuadPart) {
    GST_WARNING_OBJECT (self, "Unexpected query time");
    close_connection (self, conn);
    return;
  }

  conn->server_msg[0] = now.QuadPart;
  conn->server_msg[1] = gst_now;

  if (!WriteFileEx (conn->pipe, conn->server_msg, sizeof (conn->server_msg),
      conn, write_finished)) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    close_connection (self, conn);
  }
}

static void WINAPI
conn_finished (DWORD error_code, DWORD size, OVERLAPPED * overlap)
{
  auto conn = (ServerConn *) overlap;
  auto self = conn->server;
  auto priv = self->priv;
  LARGE_INTEGER now;

  if (error_code != ERROR_SUCCESS) {
    GST_WARNING_OBJECT (self, "WriteFileEx failed");
    close_connection (self, conn);
    return;
  }

  if (!ReadFileEx (conn->pipe, &conn->client_msg, sizeof (conn->client_msg),
      conn, read_finished)) {
    GST_WARNING_OBJECT (self, "ReadFileEx failed");
    close_connection (self, conn);
  }
}

static void
on_incoming_connection (AsioProxyClockServer * self, HANDLE pipe)
{
  auto priv = self->priv;

  auto conn_id = priv->next_conn_id;
  priv->next_conn_id++;

  auto conn = std::make_shared<ServerConn> (self, pipe, conn_id);
  priv->conn_map.insert ({conn->id, conn});

  if (G_OBJECT_TYPE (priv->clock) == GST_TYPE_SYSTEM_CLOCK)
    conn->clock_type = 1;
  else
    conn->clock_type = 0;

  if (!WriteFileEx (conn->pipe, &conn->clock_type, sizeof (conn->clock_type),
      conn.get (), conn_finished)) {
    GST_WARNING ("WriteFileEx failed");
    close_connection (self, conn.get ());
  }
}

#define PRINT_LAST_ERR(func) G_STMT_START { \
  auto last_err_ = GetLastError (); \
  auto err_ = g_win32_error_message ((gint) last_err_); \
  GST_ERROR (#func " failed with 0x%x (%s)", last_err_, err_); \
  g_free (err_); \
} G_STMT_END

static HANDLE
create_pipe (AsioProxyClockServer * self, OVERLAPPED * overlap,
    bool &io_pending)
{
  auto priv = self->priv;
  HANDLE pipe = CreateNamedPipeW (priv->address,
      PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
      PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
      PIPE_UNLIMITED_INSTANCES, sizeof (UINT64), sizeof (UINT64) * 2, 5000,
      nullptr);

  if (pipe == INVALID_HANDLE_VALUE) {
    PRINT_LAST_ERR (CreateNamedPipeA);
    return INVALID_HANDLE_VALUE;
  }

  if (ConnectNamedPipe (pipe, overlap)) {
    PRINT_LAST_ERR (ConnectNamedPipe);
    return INVALID_HANDLE_VALUE;
  }

  io_pending = false;
  guint last_err = GetLastError ();

  switch (last_err) {
    case ERROR_IO_PENDING:
      io_pending = true;
      break;
    case ERROR_PIPE_CONNECTED:
      SetEvent (overlap->hEvent);
      break;
    default:
      PRINT_LAST_ERR (ConnectNamedPipe);
      CloseHandle (pipe);
      return INVALID_HANDLE_VALUE;
  }

  return pipe;
}

static gpointer
clock_server_thread_func (AsioProxyClockServer * self)
{
  auto priv = self->priv;
  bool io_pending = false;
  HANDLE pipe;
  OVERLAPPED overlap = { };
  HANDLE waitables[2];
  DWORD task_idx = 0;
  auto task_handle = AvSetMmThreadCharacteristicsA ("Pro Audio", &task_idx);

  overlap.hEvent = CreateEvent (nullptr, TRUE, TRUE, nullptr);
  pipe = create_pipe (self, &overlap, io_pending);
  if (pipe == INVALID_HANDLE_VALUE)
    goto out;

  waitables[0] = overlap.hEvent;
  waitables[1] = priv->cancellable;

  do {
    auto ret = WaitForMultipleObjectsEx (G_N_ELEMENTS (waitables), waitables,
        FALSE, INFINITE, TRUE);

    switch (ret) {
      case WAIT_OBJECT_0:
      {
        DWORD nbytes;

        if (io_pending
            && !GetOverlappedResult (pipe, &overlap, &nbytes, FALSE)) {
          PRINT_LAST_ERR (GetOverlappedResult);
          CloseHandle (pipe);
          pipe = INVALID_HANDLE_VALUE;
          break;
        }

        on_incoming_connection (self, pipe);

        pipe = create_pipe (self, &overlap, io_pending);
        break;
      }
      case WAIT_IO_COMPLETION:
        break;
      default:
        goto out;
    }
  } while (true);

out:
  if (pipe != INVALID_HANDLE_VALUE) {
    CancelIo (pipe);
    DisconnectNamedPipe (pipe);
    CloseHandle (pipe);
  }

  CloseHandle (overlap.hEvent);

  priv->conn_map.clear ();

  if (task_handle)
    AvRevertMmThreadCharacteristics (task_handle);

  return nullptr;
}

AsioProxyClockServer *
asio_proxy_clock_server_new (const gchar * address, GstClock * clock)
{
  g_return_val_if_fail (address, nullptr);
  g_return_val_if_fail (GST_IS_CLOCK (clock), nullptr);

  auto self = (AsioProxyClockServer *)
      g_object_new (ASIO_PROXY_TYPE_CLOCK_SERVER, nullptr);
  gst_object_ref_sink (self);

  auto priv = self->priv;
  priv->clock = (GstClock *) gst_object_ref (clock);
  priv->address = (wchar_t *)
      g_utf8_to_utf16 (address, -1, nullptr, nullptr, nullptr);
  priv->loop_thread = g_thread_new ("AsioProxyClockServer",
      (GThreadFunc) clock_server_thread_func, self);

  return self;
}
