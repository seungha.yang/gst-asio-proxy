#pragma once

#include <gst/gst.h>
#include <gst/audio/audio.h>

G_BEGIN_DECLS

#define ASIO_PROXY_TYPE_SERVER (asio_proxy_server_get_type())
G_DECLARE_FINAL_TYPE (AsioProxyServer, asio_proxy_server,
    ASIO_PROXY, SERVER, GstObject);

#define ASIO_PROXY_MAX_CHANNELS 16

AsioProxyServer * asio_proxy_server_new (const gchar * address,
                                         GstClock * clock,
                                         guint num_channels,
                                         GstAudioFormat format);

G_END_DECLS